export class Master {
}
export class CountryViewModel{
    countryId:any;
    countryName:any;
    IsSaved:any;
    Status:any;
}
export class CompanyIndustryViewModel{
    companyIndustryId :any;
    companyIndustryName:any;
    IsPredefined:any;
    IsSaved:any;
    Status :any;
}
export class CompanyTypeViewModel{
   companyTypeId :any;
   companyTypeName :any;
   IsPredefined:any;
   IsSaved:any;
   Status:any;
}
export class CurrencyViewModel{
    currencyId :any;
    currencyName:any;
    currencyShortName:any;
    countryId:any;
    decimalValue:any;
    IsSaved:any;
    Status:any;
}
export class ProvinceViewModel{
    ProvinceId:any;
    provinceName:any;
    IsSaved:any;
    Status:any;
}
export class PrimaryEmailViewModel{

  PrimaryEmailId:any;
  PrimaryEmailName:any;
  IsSaved:any;
  Status:any; 
}
export class  SecondaryEmailViewModel{
    SecondaryEmailName :any;
    SecondaryEmailId:any;
    IsSaved:any;
    Status:any;
}
// export class WeeklyOffViewModel{
//     OffDayId:any;
//     OddDayName:any;
//     IsSaved:any;
//     Status:any;
    
// }