import { Component, OnInit } from '@angular/core';
import {CompanyService}  from '../../../Shared/Service/Company/company.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
//import { threadId } from 'worker_threads';
import { ToastrService } from 'ngx-toastr';
import { CountryViewModel, CompanyIndustryViewModel, CompanyTypeViewModel, CurrencyViewModel,ProvinceViewModel,
  PrimaryEmailViewModel,SecondaryEmailViewModel,} from 'src/app/Shared/Models/master/master.model';


@Component({
  selector: 'app-company-creation',
  templateUrl: './company-creation.component.html',
  styleUrls: ['./company-creation.component.scss']
})
export class CompanyCreationComponent implements OnInit {
public  createCompanyForm:FormGroup;
public createAddressForm:FormGroup;
  public submitted:boolean=false;
  public countryDDL:CountryViewModel[];
  public  currencyDDL:any;
  public  companyIndustryDDL:any;
  public companyTypeDDL:any;
  public provinceDDL:any;
  public PrimaryEmailDDL:any;
  public SecondaryEmailDDL:any;
 
  public isValid:boolean;
  public headerText: string;
  public sub:any;
  toastr: any;

  public activeTab ="Company_Info";
 
  constructor(public formbuilder:FormBuilder, public companyService:CompanyService,
    public route:ActivatedRoute,public router:Router) { }

    
  

  ngOnInit() {
    debugger;
    
   // this.sub=this.route
  //  .queryParams
  //  .subscribe(params=>{

   // })
    this.companyService.formData={
      companyName:null,
      branchName:null,
      employerCode:null,
      shortName:null,
      countryId:null,
      currencyId:null,
      companyIndustryId:null,
      companyTypeId:null,
      webSite:null,
      IsSaved:null,
      
    }
  
    
    
this.companyService.formData1={
  area:null,
  block:null,
  street:null,
  city:null,
  ProvinceId:null,
  Otherinformation:null,
  PrimaryEmailId:null,
  SecondaryEmailId:null,
  webSite:null,
  IsSaved:null,

  
}

{

    this.companyService.CountryDDL().then(res => this.countryDDL = res as CountryViewModel[]);
    this.companyService.CompanyIndustryDDL().then(res => this.companyIndustryDDL = res as CompanyIndustryViewModel[]);
    this.companyService.CompanyTypeDDL().then(res => this.companyTypeDDL = res as CompanyTypeViewModel[]);
    this.companyService.CurrencyDDL().then(res => this.currencyDDL = res as CurrencyViewModel[]);
  }
{
  this.companyService.provinceDDL().then(res=>this.provinceDDL=res as ProvinceViewModel[]);
  this.companyService.CompanyIndustryDDL().then(res => this.companyIndustryDDL = res as CompanyIndustryViewModel[]);
  this.companyService.PrimaryEmailDDL().then(res=>this.PrimaryEmailDDL=res as ProvinceViewModel[]); 
  this .companyService.SecondaryEmailDDL().then(res=>this.SecondaryEmailDDL=res as SecondaryEmailViewModel[]);
}
}

  public Address_Info(activeTab: string){
    this.activeTab = activeTab;
  }
  public BankInfo_Payroll(activeTab: string){
    this.activeTab = activeTab;
  }

  public Other_Info(activeTab: string){
    this.activeTab = activeTab;
  }
  public Logo(activeTab: string){
    this.activeTab = activeTab;
  }
  public onSubmit(){
    this.submitted=true;
    if(this.validateForm()){
      this.companyService.save().subscribe(res=>{
        if(res.IsSaved==true){
          this.toastr.success("company details added")
          this.router.navigate(['/company-creation']);
        }else if(res.IsSaved==false){
          this.toastr.warning("Already Exists")
        }
      })
    }else{
      return;
    }
  }
  
 
  public onSubmit1(){
    this.submitted=true;
    if(this.validateForm()){
      this.companyService.save().subscribe((_res: any)=>{
        if(resizeBy.IsSaved==true){
          this.toastr.success("address details added")
          this.router.navigate(['/company-creation']);
        }else if(resizeBy.IsSaved==false){
          this.toastr.warning("Alredy Exist")
        }
        
      })
    }else{
      return;
    }
  }

 
   public validateForm(){

    if(this.companyService.formData.companyName==null||this.companyService.formData.branchName==null||this.companyService.formData.employerCode
      ||this.companyService.formData.shortName==null||this.companyService.formData.countryId==null||this.companyService.formData.currencyId
     || this.companyService.formData.companyIndustryId==null||this.companyService.formData.companyTypeId==null||this.companyService.formData.webSite==null){
   }
   else{
     this.isValid=true;
   }
   return this.isValid;
  }

  public validateForm1(){
    if(this.companyService.formData1.area==null||this.companyService.formData1.block==null||this.companyService.formData1.city
      ||this.companyService.formData1.street==null||this.companyService.formData1.ProvinceId==null||this.companyService.formData1.PrimaryEmailId||this.companyService.formData1.Otherinformation
      ||this.companyService.formData1.SecondaryEmailId){
  }
  else{
    this.isValid=true;

  }
  return this.isValid;
}
}