import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Company, Address } from '../../Models/company/company.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
 
  public formData: Company;
   public formData1:Address;

  constructor(public http:HttpClient) { }
  public CountryDDL(){
    return this.http.get(environment.apiURL+'/MasterListAPI/CountryList').toPromise();
  }
  public CompanyIndustryDDL(){
    return this.http.get(environment.apiURL+'/MasterListAPI/IndustryList').toPromise();
  }
  public CompanyTypeDDL(){
    return this.http.get(environment.apiURL+'/MasterListAPI/TypeList').toPromise();
  }
  public CurrencyDDL(){
    return this.http.get(environment.apiURL+'/MasterListAPI/CurrencyList').toPromise();
  }
public provinceDDL(){
  return this.http.get(environment.apiURL+'/MasterListAPI/ProvinceList') .toPromise();
}
public PrimaryEmailDDL(){
  return this.http.get(environment.apiURL+'/MasterListAPI/PrimaryEmailList').toPromise();
}
public SecondaryEmailDDL(){
  return this.http.get(environment.apiURL+'/MasterListAPI/SecondaryEmailList').toPromise();
}
  public save(){
    var body={
      ...this.formData,
      ...this.formData1
    };
    return this.http.post<Company>(environment.apiURL+'/CompanyAPI/createCompany',body);
    return this.http.post<Address>(environment.apiURL+'/AddressAPI/createCompany',body);
  }

}